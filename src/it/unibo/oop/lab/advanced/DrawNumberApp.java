package it.unibo.oop.lab.advanced;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {

    private static final String CONFIG_PATH = "res"
            + System.getProperty("file.separator")
            + "config.yml";
    private int min;
    private int max;
    private int attempts;
    private final DrawNumber model;
    private final DrawNumberView view;

    private void loadConfig() throws IOException {
        final File configFile = new File(CONFIG_PATH);
        final List<String> configsSemiParsed;
        try {
            configsSemiParsed = Files.readAllLines(configFile.toPath());
        } catch (IOException exc) {
            System.out.println("Excpetion");
            throw exc;
        }
        final Map<String, Integer> configsParsed = new HashMap<>();
        String[] arr;
        for (final String s : configsSemiParsed) {
            arr = s.split(" ");
            configsParsed.put(arr[0], Integer.parseInt(arr[1]));
        }
        this.min = configsParsed.get("minimum:");
        this.max = configsParsed.get("maximum:");
        this.attempts = configsParsed.get("attempts:");
    }

    /**
     * 
     */
    public DrawNumberApp() {
        this.view = new DrawNumberViewImpl();
        try {
            loadConfig();
        } catch (IOException e) {
            this.view.displayError("Could not read config file!");
            quit();
        }
        this.model = new DrawNumberImpl(min, max, attempts);
        this.view.setObserver(this);
        this.view.start();
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            this.view.result(result);
        } catch (IllegalArgumentException e) {
            this.view.numberIncorrect();
        } catch (AttemptsLimitReachedException e) {
            view.limitsReached();
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        System.exit(0);
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        new DrawNumberApp();
    }

}
