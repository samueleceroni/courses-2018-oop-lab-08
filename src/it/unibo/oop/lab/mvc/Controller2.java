package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/**
 *
 */
public class Controller2 implements Controller {
    private String currentString;
    private final List<String> history = new ArrayList<String>();
    /**
     * 
     */
    @Override
    public void setNextString(final String s) {
        Objects.requireNonNull(s);
        this.currentString = s;
    }
    /**
     * @return S : nextstring
     */
    @Override
    public String getNextString() {
        return this.currentString;
    }
    /**
     * @return : the list of strings in history
     */
    @Override
    public List<String> getStringHistory() {
        final List<String> copyOfHistory = new ArrayList<String>();
        copyOfHistory.addAll(this.history);
        return copyOfHistory;
    }
    /**
     * prints the current string.
     */
    @Override
    public void printCurrentString() {
        Objects.requireNonNull(this.currentString);
        System.out.println(this.currentString);
        history.add(this.currentString);

    }

}
